var GreetingMessageManager = function (block, currentLanguage) {
	this.block = block;
	this.welcomeMessageBlock = this.block.find("#greetingMessage").find('.welcome-title');
	this.userNameBlock = this.block.find("#greetingMessage").find('a');
	this.orLabel = this.block.find("#orLabel");
	this.loginButton = this.block.find("#loginButton");
	this.currentLanguage = currentLanguage;
	this.maxLength = 14;
	this.countryAbbreviated = [ "pl", "fr" ];

	this.init();
};

GreetingMessageManager.prototype = {
	init: function() {
		this.manageGreetMessage();
	},

	manageGreetMessage: function() {
		var self = this;
		var cantShowFullGreetingMessage = $.inArray(self.currentLanguage, self.countryAbbreviated) != -1;
		var isFrench = self.currentLanguage == "fr";

		if (cantShowFullGreetingMessage) {
			if (isFrench) {
				self.orLabel.hide();
				self.loginButton.css("padding", "5px 25px");
			}
			self.welcomeMessageBlock.hide();
			self.userNameBlock.text($.trim(self.userNameBlock.text()).substring(0, self.maxLength));
		} else {
			self.userNameBlock.text($.trim(self.userNameBlock.text()).substring(0, self.maxLength));
		}
	}
};


