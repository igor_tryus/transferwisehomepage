var CalculatorAnimationsHandler = function (expandButtonText) {
	this.block = $('.home-page-calculator-wrap');
	this.expandButton = this.block.find('.tease-block a');
	this.detailsBlock = this.block.find('.calculation-details .results');
	this.currSelectorBgWrap = this.block.find('.cur');
	this.currSelector = this.block.find('.cur .ui-selectmenu');
	this.expandButtonText = expandButtonText

	this.init();
};

CalculatorAnimationsHandler.prototype = {

	init: function() {
		var self = this;

		setTimeout(self.teaseUpDown, 7000);

		this.currSelector.click(function(e) {
			self.dropDownBgRotation(e);
		});

		this.expandButton.click(function(e) {
			e.preventDefault();
			self.showDetails();
		});
	},

	showDetails: function() {
		var self = this;
		self.detailsBlock.slideToggle("fast", function(){
			if(self.detailsBlock.is(':visible')){
				LogUtils.sendGoogleAnalyticsEvent('make_transfer', 'expand_new_calculator_for_detailed_view');
			}
		});
		self.expandButton.toggleClass("down up");
		(self.expandButton.text() != "") ? self.expandButton.text("") : self.expandButton.text(self.expandButtonText)
	},

	teaseUpDown: function() {
		var self = this;
		var buttonSeeWhy = $('.tease-block a')

		buttonSeeWhy.animate({
			'background-position-y': '0px'
		}, 600 );
		buttonSeeWhy.animate({
			'background-position-y': '-3px'
		}, 600 );
	},

	dropDownBgRotation: function(e) {
		var self = this;

		$(e.target).parents('.cur').toggleClass("bg-norm bg-rotated");
	}
};

