var InitiateResponsiveHome = function () {
	this.block = $('body');
	this.window = $(window);
	this.mediaLogos = this.block.find('.partners a');
	this.sourceCurrencySelector = this.block.find('select.sourceCurrency');
	this.targetCurrencySelector = this.block.find('select.targetCurrency');

	this.init();
}

InitiateResponsiveHome.prototype = {

	init: function() {
		var self = this;

		this.window.resize(function() {
			self.manageCurrencySelectorLayout();
			self.flexSliderAppearance();
			self.manageMapMarkersAppearance();
		}).resize();

		mobileFlexSliderPartnersInit();
		mobileFlexSliderMediaHPInit();
	},

	manageCurrencySelectorLayout: function() {
		if (DeviceUtils.isDesktop()) {
			return false;
		}

		this.sourceCurrencySelector.selectmenu('destroy');
		this.targetCurrencySelector.selectmenu('destroy');
		this.sourceCurrencySelector.addClass('indent-select-calc');
		this.targetCurrencySelector.addClass('indent-select-calc');
	},

	flexSliderAppearance: function() {

		if (!mobileFlexSliderPartnersInit()) {
			flexdestroy('.flexslider');
			this.mediaLogos.hover();
		}

		if (!mobileFlexSliderMediaHPInit()) {
			flexdestroy('.flexslider-hiw');
			flexdestroy('.flexslider-trustpilot', '.progress-stars');
		}
	},

	manageMapMarkersAppearance: function() {
		mapHandler.destroy();
		if (this.window.width() > 960) {
			mapHandler.init();
		}
	}

}

