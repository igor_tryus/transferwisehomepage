 

// JavaScript Document
// embed the watcher swf
var __wtw_g_started = false;
var __wtw_dom_ready_loaded = false;

window['__wtw_lucky_site_id'] = window['__wtw_lucky_site_id'] ? window['__wtw_lucky_site_id'] : 16512;
window['__wtw_lucky_account_id'] = window['__wtw_lucky_account_id'] ? window['__wtw_lucky_account_id'] : 48285;

var WTW_Watcher = {
	use_div_extras: false,
	startup_date: new Date(),
	form_tokens: {"null":{"id":"108629846","tok":"ypVjnzDTZzqgdagZjE82rtLbtZnBCn62","orig_id":"null"},"calculateForm":{"id":"108638629","tok":"BxaXEn9B4NYcdxMAstxL2QLzk65tzwa2","orig_id":"calculateForm"}},
	extra_msg: '',
	offset_bottom: 0,
	disable_ctrl_shift : false,
	third_party_opt_out : '',
	pre_chat_question1 : 'Enter Your Name',
	pre_chat_question2 : 'Enter Your Email',
	pre_chat_question3 : '',
	no_blank_ids: false,
	is_chat_ended: false,
	detect_div_scrolls: false,
	fallback_iframe_post: false,
	socket_server : 'socket.luckyorange.com',
	
	dont_save_html: true,
	
	chat_zindex: window.__wtw_lucky_chat_zindex || null,
	
	can_record_sess : false,
	
	spy_request_queue: [],
	
	pre_chat_question1_slashed : 'Enter Your Name',
	pre_chat_question2_slashed : 'Enter Your Email',
	pre_chat_question3_slashed : '',
	
	text : { 'text_btn_yes' : 'Yes, please.',
			 'text_btn_no' : 'No thanks.',
			 'text_btn_stop' : 'Stop asking me.',
			 'text_btn_lbl' : 'chat',
			 'text_btn_yes' : 'Yes, please.'
	},
		
	use_full: 0,
	white_labeled: false,
	never_show_chat_buttons: window.__wtw_lucky_no_chat_box || true,
	no_chat_box: window.__wtw_lucky_no_chat_box || 0,
	remove_powered_by: false,
	has_form_analytics: true,
	
	ajax_addon: true,
	privacy_disable_keystrokes : true,
	 privacy_disable_mouse_clicks : false,
	 privacy_disable_mouse_movements : false,
	 privacy_disable_scrolls : false,
	 
	 encoded_ref: '',
	 show_offline_form: false,
	 scan_html_diffs: false,
	 detect_hash_changes: false,
	 my_country_code: 'UA',
	 my_country_name: 'Ukraine',
	 my_postal: '',
	 my_city: 'Cherkassy',
	 
	 my_region: 'Cherkas\'ka Oblast\'',
	 my_lon: '32.0621',
	 my_lat: '49.4285',
	 

	 
     my_ip: '109.227.125.207',
     track_all_errors: false,
    
     
	  // prechat
      
     pre_chat_ask_question: false,
         
     ga_push_poll_events: 0,
     ga_push_chat_events: 0,
     ga_event_cat: 'lo_analytics',
     
     click_to_chat_title_length: 20,
     on_poll_exit_just_hide: false,
     offline_message_length: 25,
     channel_site: '1a352a337f7bf7f1d635e0fbfc29e56b',
     channel_me: '9981e9965d7a3b45c0346cba2d720625',
     flash_save_is_loaded: false,
     
	 kick_idle_after: 60,
	 kick_idle_after_timeout_id: 0,
	 
     roomID : 16512,
	  click_to_chat_title: 'Live Help is Online.',
     offline_msg_title: 'Offline. Leave a Message.',
     do_not_record: window.__wtw_lucky_do_not_record || false,
     
     do_not_track: false,
      is_saving_mouse_coords: false,
     main_record_event_id: 0,
	
		 my_uid: '52b7f17a2fa8d109227125207',
     my_uuid: 'vis_52b7f17a2fa8d109227125207_288247',
     
     my_pass_key: 'null',
     chat_btn_alignment: ('undefined' === typeof window.__wtw_lucky_chat_align) ? 'right' : window.__wtw_lucky_chat_align,
	 
	 colors : { 'chat_subtitle' : '#999999',
				'chat_bg' : '#000000',
				'chat_title' : '#ffffff',
				'chat_subtitle' : '#999999',
				'chat_border' : '#ffffff',
				'chat_show_blinker' : '1',
				'is_chat_bg_light' : '',
				
				'chat_bg_lighter' : '#262626',
				'chat_bg_lighter2' : '#404040',
				'chat_bg_lighter3' : '#808080',
				'chat_bg_text_bg' : '#262626',
				'chat_bg_darker1' : '#000000',
				'chat_bg_darker2' : '#000000'
								
				}
};
	 
	
// English Translations
var _lo_words = {};

_lo_words.connecting = 'Connecting to chat...';
_lo_words.paging = "One moment, we're paging an agent...";
_lo_words.chatting_with = "Chatting with ";
_lo_words.no_response = "Sorry, no one responded. ";
_lo_words.page_again = "Page them again?";
_lo_words.agent_typing = ' is typing...';
_lo_words.agent_typed_text = ' has typed some text';	
_lo_words.agent_idle = ' is idle.';
_lo_words.agent_gone = ' is away from keyboard.';
_lo_words.powered_by = 'Powered By';
_lo_words.comments = 'Comments';

// Dashboard Interface
_lo_words.joined_room = 'has joined the room.';
_lo_words.left_room = 'has exited the room.';

_lo_words.operator_chat_title = 'Operator Chat';
_lo_words.visitor_chat_title = 'Visitor Chat';

_lo_words.type_here = 'Type here and press <enter> to chat with other operators.';

_lo_words.direct_link = 'Direct Link';

_lo_words.visitor_view_line = 'Visitor could view up to this line';
_lo_words.avg_viewable_screen_height = 'Most people viewed up to this line.';

_lo_words.ended_session = 'The operator ended the session.';

_lo_words.disconnected = 'Lost connection with the server...';

_lo_words.confirm_end_chat = 'Are you sure you want to end the chat session?';

_lo_words.submit_lbl = 'Submit';



	
	
	
		// Load Main JS
	(function() {
		
		var loaded_search = false;
		var loaded_diff = false;
		var lo_loaded = false;
		var lo_loading = false;
		var lo_metric_loaded = false;
		load_search();
		
	
		function load_lo()
		{
			try 
			{ 
				if (lo_loading == false)
				{
					lo_loading = true;
					
					// load metric api
					load_script( window.lucky_pre_path + 'luckyorange.com/a.js?nc=2', function(){ 
							lo_metric_loaded = true;
						});
					
					
					if (document.location.href.indexOf("__no_min_debug") > -1)
					{
						load_script( window.lucky_pre_path + 'luckyorange.com/lo.js?nc=34', function(){ 
							lo_loaded = true;
						});	
					}
					else
					{ 
						load_script( window.lucky_pre_path + 'luckyorange.com/lo.min.js?nc=143', function(){ 
							lo_loaded = true;
							});	
					}
				}
			}
			catch(ex)
			{ 
			}
		}
	
		function check_done()
		{
			// load the final script only if pre-reqs are loaded
			if (loaded_search && loaded_diff)
			{
				load_lo();
			}
		}
		
		function load_search()
		{
			try
			{		
				// search engines
				load_script( window.lucky_pre_path + 'luckyorange.com/js/engines.min.js', function(){ 
					loaded_search = true;					
					check_done();
				});
				
				// scan diffs
				if (WTW_Watcher.scan_html_diffs)
				{
					load_script( window.lucky_pre_path + 'luckyorange.com/js/diff_match_patch.min.js', function(){ 
						loaded_diff = true;
						check_done();
					});	
				}
				else
				{				
					loaded_diff = true;
					check_done();
				}
											
				
			}
			catch(ex)
			{
			}
		}
		
		function load_script(url, callback)
		{
			try
			{
				var head = document.head || document.getElementsByTagName( "head" )[0] || document.documentElement;
					var script = document.createElement("script");
							  
					script.async = true;
					script.charset = 'utf-8';
					script.type = 'text/javascript';
					script.src = url;
					
					// Attach handlers for all browsers
					script.onload = script.onreadystatechange = function( _, isAbort ) {
						
						if ( isAbort || !script.readyState || /loaded|complete/.test( script.readyState ) ) {

							// Handle memory leak in IE
							script.onload = script.onreadystatechange = null;
						
							if (typeof(callback) === 'function')
							{
								callback();
							}
						}
		
					}
					
					head.insertBefore( script, head.firstChild );
			}
			catch(ex)
			{
			}
		}
		
	})();
	