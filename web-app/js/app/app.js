/*#######################################################################
  /app
      /controllers
      /services
      /views

  #######################################################################*/

var app = angular.module('calculatorApp', ['ngRoute']);

//This configures the routes and associates each route with a view and a controller
app.config(function ($routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'js/app/views/calculator.html',
            controller: 'calculatorController'
        })
});




