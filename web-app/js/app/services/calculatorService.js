﻿app.service('calculatorService', function ($http) {
    this.getCurrencies = function () {
        var currencies = $http.get('/CurrencyCalculator/currencies').then(function (response) {
            return response.data;
        },
        function () {
            if (confirm("Oops, that's new. Please try again by reloading this page. In case if issue remains - let us know and we gladly help you. Would you like to reload this page?")) {
                window.location.reload(true);
            }
        });
        return currencies
    };

    this.calculate = function (data) {
        var result = $http.post('/CurrencyCalculator/calculate', data).then(function (response) {
                return response.data;
            },
            function () {
                if (confirm("Oops, that's new. Please try again by reloading this page. In case if issue remains - let us know and we gladly help you. Would you like to reload this page?")) {
                    window.location.reload(true);
                }
            });

        return result
    };

});