﻿app.controller('calculatorController', function ($scope, calculatorService) {
    function init() {
        //Get list of currencies from server
        calculatorService.getCurrencies().then(function (results) {
            //Initialize variables
            $scope.currencies = results;
            $scope.sourceValue = 1000.0;
            $scope.sourceCurrency = 2; //GBP
            $scope.targetCurrency = 1; //EUR

            //Calculate target value
            $scope.calculateTarget();
        });
        $scope.state = "source";
    }

    //Update UI selectors
    function updateSelectors() {
        setTimeout(function () {
            jQuery('#calculateForm select').selectmenu();
        }, 0)
    }

    $scope.$watch('currencies', function() {
        updateSelectors();
    }); // initialize the watch

    $scope.calculateTarget = function() {
        var data = {};
        if ($scope.sourceCurrency == $scope.targetCurrency) {
            ($scope.currencies.length < $scope.targetCurrency + 1) ? $scope.targetCurrency = 1 : $scope.targetCurrency += 1;
            updateSelectors();
        }
        data.sourceValue = parseFloat($scope.sourceValue);
        data.sourceCurrency = $scope.sourceCurrency;
        data.targetCurrency = $scope.targetCurrency;
        calculatorService.calculate(data).then(function (results) {
            $scope.targetValue = results.target_value;
            updateCalculationDetails(results);
        });
    }

    $scope.calculateSource = function() {
        var data = {};
        if ($scope.sourceCurrency == $scope.targetCurrency) {
            ($scope.currencies.length < $scope.sourceCurrency + 1) ? $scope.sourceCurrency = 1 : $scope.sourceCurrency += 1;
            updateSelectors();
        }
        data.sourceValue = parseFloat($scope.targetValue);
        data.sourceCurrency = $scope.targetCurrency;
        data.targetCurrency = $scope.sourceCurrency;
        calculatorService.calculate(data).then(function (results) {
            $scope.sourceValue = results.target_value;
            updateCalculationDetails(results);
        });
    }

    function updateCalculationDetails(responce) {
        $scope.calculationResults = responce;
    }

    $scope.setState = function(state) {
        $scope.state = state
    }

    init();
});