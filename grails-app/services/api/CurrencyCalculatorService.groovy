package api

import grails.transaction.Transactional

@Transactional
class CurrencyCalculatorService {

    def calculate(def data) {
        Currency sourceCurrency = Currency.findById(data.sourceCurrency)
        Currency targetCurrency = Currency.findById(data.targetCurrency)

        def transferwiseRate = sourceCurrency.getTransferWiseRate(targetCurrency.toString())
        def averagebankRate = sourceCurrency.getBankRate(targetCurrency.toString())

        float transferwiseFee = data.sourceValue * 0.01 < 1.0 ? 1.0 : data.sourceValue * 0.01; //ONLY for demonstration
        float averagebankFee = data.sourceValue * 0.05 < 5.0 ? 5.0 : data.sourceValue * 0.05;  //ONLY for demonstration

        def targetValue = (data.sourceValue ? data.sourceValue.toFloat() : 0.0) * transferwiseRate - transferwiseFee
        def averagebankValue = (data.sourceValue ? data.sourceValue.toFloat() : 0.0) * averagebankRate - averagebankFee

        def result = [:]
        result['target_value'] = targetValue.toFloat().round(2)
        result['transferwise_rate'] = transferwiseRate
        result['averagebank_rate'] = averagebankRate
        result['transferwise_fee'] = sourceCurrency.symbol + " " + transferwiseFee.round(2)
        result['averagebank_fee'] = sourceCurrency.symbol + " " + averagebankFee.round(2)

        result['transferwise_fee_note'] = "1% for all transfers"
        result['averagebank_fee_note'] = "bank fee + hidden profit"

        result['transferwise_you_get'] = targetCurrency.symbol + " " + targetValue.toFloat().round(2)
        result['averagebank_you_get'] = targetCurrency.symbol + " " + averagebankValue.toFloat().round(2)

        result['saving_amount'] = targetCurrency.symbol + " " + (targetValue.toFloat() - averagebankValue.toFloat()).round(2)

        return result
    }
}

public enum Currency {
    //Stub for currencies
    EUR(1, '€', ['GBP':0.83668, 'PLN':4.15428, 'CHF':1.22720], ['GBP':0.80739, 'PLN':4.00888, 'CHF':1.18425]),
    GBP(2, '£', ['EUR':1.19512, 'PLN':4.96487, 'CHF':1.46666], ['EUR':1.15329, 'PLN':4.79110, 'CHF':1.41532]),
    PLN(3, 'zł', ['EUR':0.24071, 'GBP':0.20143, 'CHF':0.29539], ['EUR':0.23228, 'GBP':0.19438, 'CHF':0.28505])

    Integer id
    String symbol
    Map twRates
    Map bankRates

    Currency(Integer id, String symbol, Map twRates, Map bankRates) {
        this.id = id
        this.symbol = symbol
        this.twRates = twRates
        this.bankRates = bankRates
    }

    public static findById(Integer id) {
        values().find{it.id == id}
    }

    public static getJsonMap() {
        values().collect{ it -> [name: it.toString(), id: it.id] }
    }

    public getTransferWiseRate(String currencyCode) {
        twRates[currencyCode]
    }

    public getBankRate(String currencyCode) {
        bankRates[currencyCode]
    }
}
