package api

import grails.converters.JSON

class CurrencyCalculatorController {
    def currencyCalculatorService //Initialize service for calculator

    def calculate() {
        render currencyCalculatorService.calculate(request.JSON) as JSON
    }

    def currencies () {
        render Currency.getJsonMap() as JSON
    }
}
