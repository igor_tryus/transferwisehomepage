<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Transfer Money Online | Send Money Abroad with TransferWise</title>

        <script src="${resource(dir: 'js', file: 'foundation-A26091-0f7a-4f32-8a77-ffc0d8bc79312.js')}" type="text/javascript" async=""></script>
        <script src="${resource(dir: 'js', file: 'foundation-tags-SD50-b426-44c4-89ec-cebea42675902.js')}" async="" defer="" type="text/javascript"></script>
        <script type="text/javascript" async="" src="${resource(dir: 'js', file: 'ga.js')}"></script>
        <script async="" charset="utf-8" type="text/javascript" src="${resource(dir: 'js', file: 'a.js')}"></script>
        <script async="" charset="utf-8" type="text/javascript" src="${resource(dir: 'js', file: 'engines.min.js')}"></script>

        <!-- Meta -->

        <meta name="robots" content="noodp">
        <meta name="version" content="2.51.648r">
        <meta name="keywords" content="">
        <meta name="apple-itunes-app" content="app-id=612261027">

        <script type="text/javascript">
            var mixPanelToken = '8ba4a7a5182f05e0a79ded57d5d2f051';
            var baseUri = '/';
        </script>

        <script type="text/javascript">
            function initMixPanel() {
                if (!window.mixPanelCookie) {
                    mixpanel.track('first visit', {url: window.location.pathname});
                }
            }
        </script>

        <!-- Microsoft smarties -->
        <meta http-equiv="ImageResize" content="no">
        <meta http-equiv="ImageToolbar" content="no">
        <meta name="MSSmartTagsPreventParsing" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Default stylesheets -->
        <link rel="shortcut icon" href="https://transferwise.com/static/images/favicon.ico" type="image/x-icon">
        <link rel="image_src" href="https://transferwise.com/static/images/TW_logo.png">

        <!--[if lt IE 9]>
                <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <![endif]-->

        <meta property="og:image" content="http://transferwise.com/images/fb-og-logo.png">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-jui_head.css')}" type="text/css" rel="stylesheet" media="screen">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-base-css_head.css')}" type="text/css" rel="stylesheet" media="screen">

        <!--[if lt IE 8]><link href="/static/css/blueprint/2.51.648r_ie.css" type="text/css" rel="stylesheet" media="screen" /><![endif]-->
        <!--[if IE]><link href="/static/css/2.51.648r_ie.css" type="text/css" rel="stylesheet" media="screen" /><![endif]-->
        <!--[if IE 8]><link href="/static/css/2.51.648r_ie8.css" type="text/css" rel="stylesheet" media="screen" /><![endif]-->

        <script src="${resource(dir: 'js', file: '2.51.648r_jquery-1.7.1.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-jui_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-scrolldepth_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-header_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-ajaxSetup_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-bundle_baseJs_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-messages_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-cookie_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-calculatorHelpOpener_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-fancybox_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-validation_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-flexslider_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-moment_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-index_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-homePageCalculator_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-bundle_responsive_index_defer.js')}" type="text/javascript"></script>
        <script src="${resource(dir: 'js', file: '2.51.648r_bundle-bundle_baseJs_head.js')}" type="text/javascript"></script>
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-core_head.css')}" type="text/css" rel="stylesheet" media="screen">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-responsive_notifications_head.css')}" type="text/css" rel="stylesheet" media="screen and (min-width:50px) and (max-width:960px)">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-responsive_m_head.css')}" type="text/css" rel="stylesheet" media="screen and (min-width:50px) and (max-width:767px)">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-responsive_t_head.css')}" type="text/css" rel="stylesheet" media="screen and (min-width:768px) and (max-width:960px)">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-fancybox_head.css')}" type="text/css" rel="stylesheet" media="screen, projection">
        <script src="${resource(dir: 'js', file: '2.51.648r_luckyOrange.js')}" type="text/javascript"></script>
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-index_head.css')}" type="text/css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="${resource(dir: 'js', file: 'foundation-A26091-0f7a-4f32-8a77-ffc0d8bc79312.js')}"></script>
        <script type="text/javascript" src="${resource(dir: 'js', file: 'conversion.js')}"></script>

        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-homePageCalculator_head.css')}" type="text/css" rel="stylesheet" media="screen, projection">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-responsive_index_m_head.css')}" type="text/css" rel="stylesheet" media="screen and (min-width:50px) and (max-width:767px)">
        <link href="${resource(dir: 'css', file: '2.51.648r_bundle-responsive_index_t_head.css')}" type="text/css" rel="stylesheet" media="screen and (min-width:768px) and (max-width:960px)">
        <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: '2.51.648r_tooltipStyles.css')}">
        <script type="text/javascript" src="${resource(dir: 'js', file: '2.51.648r_tooltip-min.js')}"></script>
        <meta name="google-site-verification" content="d9ZnJG0yqvxWCi-eMnG1jOGvKa7TkO5YyFN35YYyWcc">
        <link rel="alternate" hreflang="de" href="https://transferwise.com/de">
        <meta name="title" content="Transfer Money Online | Send Money Abroad with TransferWise">
        <meta name="description" content="Banks charge a lot for overseas transfers. We don&#39;t. Transfer money abroad easily and quickly with our low cost money transfers.">
        <meta property="twitter:account_id" content="1510092490">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@transferwise">
        <meta name="twitter:title" content="The clever new way to send money abroad">
        <meta name="twitter:description" content="Banks sting you with hidden fees when you send money abroad. But that’s yesterday’s problem. Say hello to TransferWise, the clever new way to move your money.">
        <meta name="twitter:image:src" content="http://transferwise.com/images/social/byebyebankfees.jpg">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script type="text/javascript" src="${resource(dir: 'js', file: 'tog0vmo.js')}"></script>
        <style type="text/css">.tk-ff-meta-web-pro{font-family:"ff-meta-web-pro",sans-serif;}</style>
        <link rel="stylesheet" href="https://use.typekit.com/k/tog0vmo-d.css?3bb2a6e53c9684ffdc9a9bf21d5b2a62e197b8a35fd0ae87d83f8c7f4e5004202b800b2024c5b6dc162989af35959f27181a9a1cd58d51124f01e55466797fdade6c5ddf3366468cc0d6da569811d1f9fbf6b4cdb8e6596ba0858168eb80e2f768f4cf68a3b1ede607581d770370f0e3134668575431d4e061b6ebbae68d18a9ab38fd29e5f66070bbf170235b22bfcbdf053b">
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
        <style type="text/css"></style><style type="text/css">body .SnapABug_Button{cursor:pointer;cursor:hand;overflow:hidden;position:fixed;_position:absolute;display:block;bottom:0px;left:90%;_left:expression(eval(document.body.scrollLeft)+1229);z-index:2147000000;margin:0;padding:0;border-collapse:collapse;border-spacing:0;border:none;outline:none;font-size:0px;line-height:0px;} @media print{body .SnapABug_Button{display:none;}}</style>

        <!--<script async="true" type="text/javascript" src="https://s.adroll.com/j/roundtrip.js"></script>-->
        <link rel="stylesheet" type="text/css" media="all" href="${resource(dir: 'css', file: 'style2.css')}">
        <style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}

        .fb_invisible{display:none}
        .fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}
        .fb_reset > div{overflow:hidden}
        .fb_link img{border:none}
        .fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}
        .fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}
        .fb_dialog_content{background:#fff;color:#333}
        .fb_dialog_close_icon{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;_background-image:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif);cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px;top:8px\9;right:7px\9}
        .fb_dialog_mobile .fb_dialog_close_icon{top:5px;left:5px;right:auto}
        .fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}
        .fb_dialog_close_icon:hover{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent;_background-image:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif)}
        .fb_dialog_close_icon:active{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent;_background-image:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif)}
        .fb_dialog_loader{background-color:#f2f2f2;border:1px solid #606060;font-size:24px;padding:20px}
        .fb_dialog_top_left,
        .fb_dialog_top_right,
        .fb_dialog_bottom_left,
        .fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}
        .fb_dialog_top_left{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 0;left:-10px;top:-10px}
        .fb_dialog_top_right{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -10px;right:-10px;top:-10px}
        .fb_dialog_bottom_left{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -20px;bottom:-10px;left:-10px}
        .fb_dialog_bottom_right{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -30px;right:-10px;bottom:-10px}
        .fb_dialog_vert_left,
        .fb_dialog_vert_right,
        .fb_dialog_horiz_top,
        .fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}
        .fb_dialog_vert_left,
        .fb_dialog_vert_right{width:10px;height:100%}
        .fb_dialog_vert_left{margin-left:-10px}
        .fb_dialog_vert_right{right:0;margin-right:-10px}
        .fb_dialog_horiz_top,
        .fb_dialog_horiz_bottom{width:100%;height:10px}
        .fb_dialog_horiz_top{margin-top:-10px}
        .fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}
        .fb_dialog_iframe{line-height:0}
        .fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #3b5998;color:#fff;font-size:14px;font-weight:bold;margin:0}
        .fb_dialog_content .dialog_title > span{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/yd/r/Cou7n-nqK52.gif)
        no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}
        body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}
        .fb_dialog.fb_dialog_mobile.loading{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/ya/r/3rhSv5V8j3o.gif)
        white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}
        .fb_dialog.fb_dialog_mobile.loading.centered{max-height:590px;min-height:590px;max-width:500px;min-width:500px}
        #fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .45);position:absolute;left:0;top:0;width:100%;min-height:100%;z-index:10000}
        #fb-root #fb_dialog_ipad_overlay.hidden{display:none}
        .fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}
        .fb_dialog_content .dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}
        .fb_dialog_content .dialog_header table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%
        }
        .fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px
        }
        .fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px
        }
        .fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6),
        color-stop(0.5, #355492), to(#2A4887));border:1px solid #29447e;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset,
        rgba(255, 255, 255, .167969) 0 1px 0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}
        .fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}
        .fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}
        .fb_dialog_content .dialog_content{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}
        .fb_dialog_content .dialog_footer{background:#f2f2f2;border:1px solid #555;border-top-color:#ccc;height:40px}
        #fb_dialog_loader_close{float:left}
        .fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}
        .fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}
        .fb_iframe_widget{display:inline-block;position:relative}
        .fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}
        .fb_iframe_widget iframe{position:absolute}
        .fb_iframe_widget_lift{z-index:1}
        .fb_hide_iframes iframe{position:relative;left:-10000px}
        .fb_iframe_widget_loader{position:relative;display:inline-block}
        .fb_iframe_widget_fluid{display:inline}
        .fb_iframe_widget_fluid span{width:100%}
        .fb_iframe_widget_loader iframe{min-height:32px;z-index:2;zoom:1}
        .fb_iframe_widget_loader .FB_Loader{background:url(https://fbstatic-a.akamaihd.net/rsrc.php/v2/y9/r/jKEcVPZFk-2.gif) no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}
        .fb_connect_bar_container div,
        .fb_connect_bar_container span,
        .fb_connect_bar_container a,
        .fb_connect_bar_container img,
        .fb_connect_bar_container strong{background:none;border-spacing:0;border:0;direction:ltr;font-style:normal;font-variant:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal;vertical-align:baseline}
        .fb_connect_bar_container{position:fixed;left:0 !important;right:0 !important;height:42px !important;padding:0 25px !important;margin:0 !important;vertical-align:middle !important;border-bottom:1px solid #333 !important;background:#3b5998 !important;z-index:99999999 !important;overflow:hidden !important}
        .fb_connect_bar_container_ie6{position:absolute;top:expression(document.compatMode=="CSS1Compat"? document.documentElement.scrollTop+"px":body.scrollTop+"px")}
        .fb_connect_bar{position:relative;margin:auto;height:100%;width:100%;padding:6px 0 0 0 !important;background:none;color:#fff !important;font-family:"lucida grande", tahoma, verdana, arial, sans-serif !important;font-size:13px !important;font-style:normal !important;font-variant:normal !important;font-weight:normal !important;letter-spacing:normal !important;line-height:1 !important;text-decoration:none !important;text-indent:0 !important;text-shadow:none !important;text-transform:none !important;white-space:normal !important;word-spacing:normal !important}
        .fb_connect_bar a:hover{color:#fff}
        .fb_connect_bar .fb_profile img{height:30px;width:30px;vertical-align:middle;margin:0 6px 5px 0}
        .fb_connect_bar div a,
        .fb_connect_bar span,
        .fb_connect_bar span a{color:#bac6da;font-size:11px;text-decoration:none}
        .fb_connect_bar .fb_buttons{float:right;margin-top:7px}
        .fb_edge_widget_with_comment{position:relative;*z-index:1000}
        .fb_edge_widget_with_comment span.fb_edge_comment_widget{position:absolute}
        .fb_edge_widget_with_comment span.fb_send_button_form_widget{z-index:1}
        .fb_edge_widget_with_comment span.fb_send_button_form_widget .FB_Loader{left:0;top:1px;margin-top:6px;margin-left:0;background-position:50% 50%;background-color:#fff;height:150px;width:394px;border:1px #666 solid;border-bottom:2px solid #283e6c;z-index:1}
        .fb_edge_widget_with_comment span.fb_send_button_form_widget.dark .FB_Loader{background-color:#000;border-bottom:2px solid #ccc}
        .fb_edge_widget_with_comment span.fb_send_button_form_widget.siderender
            .FB_Loader{margin-top:0}
        .fbpluginrecommendationsbarleft,
        .fbpluginrecommendationsbarright{position:fixed !important;bottom:0;z-index:999}
        .fbpluginrecommendationsbarleft{left:10px}
        .fbpluginrecommendationsbarright{right:10px}</style>

        <script src="${resource(dir: 'js', file: 'angular.min.js')}"></script>
        <script src="${resource(dir: 'js', file: 'angular-route.min.js')}"></script>
        <script src="${resource(dir: 'js', file: 'app/app.js')}"></script>
        <script src="${resource(dir: 'js', file: 'app/controllers/controllers.js')}"></script>
        <script src="${resource(dir: 'js', file: 'app/services/calculatorService.js')}"></script>

		<g:layoutHead/>
		<g:javascript library="application"/>
		<r:layoutResources />
	</head>
	<body data-ng-app="calculatorApp">
		<g:layoutBody/>
		<r:layoutResources />
	</body>
</html>
