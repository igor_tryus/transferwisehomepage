<!DOCTYPE html>
<html>
	<head>
        <meta name="layout" content="main"/>
	</head>

    <body>
        <div class="header ">
            <div class="container">
                <div class="logo event_logo"><a href="https://transferwise.com/en"></a></div>
                <div class="nav-menu-button"><a href="https://transferwise.com/#"></a></div>
                <div class="nav-indent"></div>

                <ul class="nav b">
                    <li><a href="" class="active event_how_it_works">Send money</a></li>
                    <li><a href="https://transferwise.com/getpaid" class=" business-link">Receive Money</a></li>
                    <li><a href="https://transferwise.com/support" class="">Help &amp; Support</a></li>
                    <li><a href="https://transferwise.com/en/about" class="">About Us</a></li>
                    <li><a href="https://transferwise.com/login/index" class="in-hide login-mobile left " onclick="this.blur();">Log in</a></li>
                    <li class="empty responsive-clear">&nbsp;</li>
                </ul>
                <div class="lang">
                    <a href="https://transferwise.com/#" class="flag GB choose-link"><br><span class="arrow-down-lan"></span></a>
                    <ul class="language-list-ul">
                        <li><a href="https://transferwise.com/en"><div class="flag GB"></div><div class="language-text">EN</div></a></li>
                        <li> <a href="https://transferwise.com/pl"><div class="flag PL"></div> <div class="language-text">PL</div></a></li>
                        <li><a href="https://transferwise.com/de"><div class="flag DE"></div><div class="language-text">DE</div></a></li>
                        <li><a href="https://transferwise.com/es"><div class="flag ES"></div><div class="language-text">ES</div></a></li>
                        <li><a href="https://transferwise.com/it"><div class="flag IT"></div><div class="language-text">IT</div></a></li>
                    </ul>
                </div>
                <div class="login">
                    <a href="https://transferwise.com/login/index" class="link" onclick="this.blur();">Log in</a>
                    <span id="orLabel">or</span>
                    <a href="https://transferwise.com/register/index" class="event-signup-click button orange middle">sign up</a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="responsive-clear"></div>
        <div class="map">
            <div class="map-background"></div>
            <div class="dynamic-map-holder"><div class="map-wrapper" style="width: 750px; height: 100%;"><div class="marker" style="left: 1px; top: 325px; opacity: 0.3;"><div class="left-wrapper"><em class="flag ES"></em><div class="text">£167 to €</div><div class="time">moments ago</div></div></div><div class="marker" style="left: 26px; top: 105px; opacity: 0.19200000762939456;"><div class="left-wrapper"><em class="flag GB"></em><div class="text">Ft70k to £</div><div class="time">moments ago</div></div></div></div></div>
            <div class="container">
                <div class="map-text tab1">
                    <div class="top10">
                    </div>

                    <div class="left-block">
                        <h1 class="b">Hey, hidden fees.<br>Your secret’s out.</h1>
                        <p class="top20 block-desktop block-tablet">Sending money abroad is deceptively expensive, thanks to the hidden fees we’ve all been forced to pay. Now TransferWise lets expats, foreign students and businesses transfer money wherever it's needed, at the lowest possible cost. No hidden fees, no headache.<br></p>
                        <p class="top10 block-mobile">TransferWise lets you move your money abroad at the lowest possible cost. No hidden fees, no headache.<br></p>
                        <div class="new-calculator-padding"></div>
                    </div>

                    <div class="right-block" >
                        <div ng-view></div>
                        <noscript>
                            &lt;img src="https://2.xg4ken.com/media/redir.php?track=1&amp;token=e12d62e4-354d-439b-8b6d-b8fc33702a4f&amp;type=calculator&amp;val=0.0&amp;orderId=&amp;promoCode=&amp;valueCurrency=GBP&amp;GCID=&amp;kw=&amp;product=" width="1" height="1"&gt;
                        </noscript>
                    </div>
                    <div class="clear"></div>

                    <div class="skypal-container ">
                        <a href="https://transferwise.com/support/our-past-lives" class="skype-paypal-links" target="_blank">
                            <div class="skype-paypal-badge">
                                <div class="from-text">From the people who built</div>
                                <img src="/static/images/skype_logo.png" class="skype-logo">
                                <div class="from-text">&amp;</div>
                                <img src="/static/images/paypal_logo.png" class="paypal-logo">
                            </div>
                        </a>
                    </div>

                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="body">
        <div class="container home-media">

        <div class="media top25">
            <div class="video left">
                <div class="video-embed">
                    <div class="player">
                        <div id="cover-pic-wrap"><img src="/static/images/video_shot.jpg" class="video-shot"></div>
                    </div>
                </div>
            </div>

            <div class="stories col55">
                <div class="flexslider-stories">
                    <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 1600%; margin-left: -1056px;"><li class="clone" style="width: 528px; float: left; display: block;">
                        <div class="text col65">
                            <h4>Saving £120 per month in bank fees</h4>
                            <p>I am regularly sending money to France and Austria. It is crazy that I used to queue up at the bank branch, pay £14 or £30 per transfer and get a poor exchange rate.</p>
                            <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                        </div>
                        <div class="customer col30">
                            <img src="/static/images/cust-stories-alexandra.png" class="top10">
                            <p class="name">Alexandra Thiebaut</p>
                            <p class="location">Lives in London, sends money back to France</p>
                        </div>
                    </li>
                        <li style="width: 528px; float: left; display: block;" class="">
                            <div class="text col65">
                                <h4>I would definitely recommend it to others!</h4>
                                <p>I'm an expat myself, and my business helps people relocate to a new country, so TransferWise is a perfect fit. We've saved loads paying our contractors in other parts of Europe, and I would definitely recommend it to others!</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-brynne.png" class="top10">
                                <p class="name">Brynne H, moveguides.com</p>
                                <p class="location">Lives in London, makes personal and business transfers</p>
                            </div>
                        </li>
                        <li style="width: 528px; float: left; display: block;" class="flex-active-slide">
                            <div class="text col65">
                                <h4>I have recommended your service to all my friends and will continue doing so!</h4>
                                <p>I have been using the service for 12-18 months - I have never encountered any problem and was impressed by the quality of your customer service.</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-damien.png" class="top10">
                                <p class="name">Damien A, Entrepreneur</p>
                                <p class="location">Transfers money between the UK and France</p>
                            </div>
                        </li>
                        <li style="width: 528px; float: left; display: block;" class="">
                            <div class="text col65">
                                <h4>I no longer have to worry about expensive money transfer fees &amp; unfair exchange rates</h4>
                                <p>This is the perfect tool for the fast growing number of globetrotters and international-minded people around the world. I recommend this service to anyone and everyone I meet and it's honestly the best and most practical tool online next to Skype.</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">
                                    More stories
                                </a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-wendi.png" class="top10">
                                <p class="name">Wendi Li. Proud globetrotter</p>
                                <p class="location">Sends money to Germany and the US</p>
                            </div>
                        </li>
                        <li style="width: 528px; float: left; display: block;" class="">
                            <div class="text col65">
                                <h4>I've not used another currency exchange company since I discovered TransferWise</h4>
                                <p>I have found TransferWise has the best exchange rate, even allowing for the small fee, and therefore the best value. It is easy, very quick, and I am very satisfied with the service.</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-geoff.png" class="top10">
                                <p class="name">Geoff G.</p>
                                <p class="location">Happily retired, with a 2nd home in France</p>
                            </div>
                        </li>
                        <li style="width: 528px; float: left; display: block;" class="">
                            <div class="text col65">
                                <h4>Beats our bank hands down in cost, speed and convenience!</h4>
                                <p>When running a young business you can't waste your time or your money. We are paying our developers in Portugal and are most impressed by the speed at which TransferWise makes our payments.</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-celso.png" class="top10">
                                <p class="name">Celso Pinto, gosimpletax.com</p>
                                <p class="location">Lives in London, sends money to Portugal and the US</p>
                            </div>
                        </li>
                        <li style="width: 528px; float: left; display: block;" class="">
                            <div class="text col65">
                                <h4>Saving £120 per month in bank fees</h4>
                                <p>I am regularly sending money to France and Austria. It is crazy that I used to queue up at the bank branch, pay £14 or £30 per transfer and get a poor exchange rate.</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-alexandra.png" class="top10">
                                <p class="name">Alexandra Thiebaut</p>
                                <p class="location">Lives in London, sends money back to France</p>
                            </div>
                        </li>
                        <li style="width: 528px; float: left; display: block;" class="clone">
                            <div class="text col65">
                                <h4>I would definitely recommend it to others!</h4>
                                <p>I'm an expat myself, and my business helps people relocate to a new country, so TransferWise is a perfect fit. We've saved loads paying our contractors in other parts of Europe, and I would definitely recommend it to others!</p>
                                <a class="button stories" href="https://transferwise.com/#" style="display: none;">More stories</a>
                            </div>
                            <div class="customer col30">
                                <img src="/static/images/cust-stories-brynne.png" class="top10">
                                <p class="name">Brynne H, moveguides.com</p>
                                <p class="location">Lives in London, makes personal and business transfers</p>
                            </div>
                        </li></ul></div><ol class="flex-control-nav flex-control-paging"><li><a class="">1</a></li><li><a class="flex-active">2</a></li><li><a class="">3</a></li><li><a class="">4</a></li><li><a class="">5</a></li><li><a class="">6</a></li></ol><ul class="flex-direction-nav"><li><a class="flex-prev" href="https://transferwise.com/#">Previous</a></li><li><a class="flex-next" href="https://transferwise.com/#">Next</a></li></ul></div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="quotes top40">
            <p class="quote"><span class="symbol">“</span>Using the Skype principle to slash the rates that people pay to send money abroad<span class="symbol">”</span></p>
            <p class="author">The Economist</p>
            <div class="partners">
                <div class="flexslider">
                    <ul class="slides event_partners">
                        <li>
                            <a href="http://www.ft.com/cms/s/0/920a5a56-cb7c-11e1-911e-00144feabdc0.html" id="7" title="Transferwise.com featured on FT" target="_blank" class="featuredFt">
                                <input type="hidden" name="quote" class="quote-text" value="TransferWise is looking to use the internet to spark a digital revolution in financial services" id="quote">
                                <input type="hidden" name="author" class="author-text" value="Financial Times" id="author">
                            </a>
                        </li>
                        <li>
                            <a href="http://www.wired.co.uk/magazine/archive/2011/08/start/data-banking" id="5" title="Transferwise.com featured on Wired" target="_blank" class="wired">
                                <input type="hidden" name="quote" class="quote-text" value="Smart tech and sharp thinking are disrupting high-street banks" id="quote">
                                <input type="hidden" name="author" class="author-text" value="Wired" id="author">
                            </a>
                        </li>
                        <li>
                            <a href="http://www.guardian.co.uk/uk/2012/jul/08/east-london-20-hottest-tech-companies" id="8" title="Transferwise.com featured on TheGuardian" target="_blank" class="theguardian">
                                <input type="hidden" name="quote" class="quote-text" value="TransferWise has every chance of carving out a sizeable niche in the currency exchange market" id="quote">
                                <input type="hidden" name="author" class="author-text" value="The Guardian" id="author">
                            </a>
                        </li>
                        <li>
                            <a href="http://www.economist.com/blogs/schumpeter/2013/01/international-money-transfers" id="9" title="Transferwise.com featured on The Economist" target="_blank" class="theEconomist">
                                <input type="hidden" name="quote" class="quote-text" value="Using the Skype principle to slash the rates that people pay to send money abroad" id="quote">
                                <input type="hidden" name="author" class="author-text" value="The Economist" id="author">
                            </a>
                        </li>
                        <li>
                            <a href="http://www.bbc.co.uk/news/business-14808977" id="6" title="Transferwise.com featured on BBC" target="_blank" class="bbc">
                                <input type="hidden" name="quote" class="quote-text" value="TransferWise cuts out banks and their commissions" id="quote">
                                <input type="hidden" name="author" class="author-text" value="BBC" id="author">
                            </a>
                        </li>
                        <li>
                            <a href="http://www.nytimes.com/external/venturebeat/2011/02/03/03venturebeat-ex-skyper-shakes-up-currency-exchange-93758.html?ref=technology" id="2" title="Transferwise.com featured on The New York Times" target="_blank" class="newYorkTimes">
                                <input type="hidden" name="quote" class="quote-text" value="Ex-Skyper Shakes Up Currency Exchange" id="quote">
                                <input type="hidden" name="author" class="author-text" value="NYTimes" id="author">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="how-it-works top25" id="howItWorks">
            <h4>This is how it works. Send your first payment in minutes. No fluff, no jargon, full transparency.</h4>
            <div class="blocks">
                <div class="flexslider-hiw">
                    <ul class="slides">
                        <li class="col30">
                            <div class="wrap-hiw">
                                <div class="media-img">
                                    <img src="/static/images/media-block1.png">
                                </div>
                                <h3>Create your transfer</h3>
                                First you create a transfer on our website. This takes only a couple of minutes – you just tell us how much you’re transferring and where to.
                            </div>
                        </li>
                        <li class="col30">
                            <div class="wrap-hiw">
                                <div class="media-img">
                                    <img src="/static/images/media-block2.png">
                                </div>
                                <h3>Your money</h3>
                                Next, send your money to TransferWise. You can do this in two ways: upload using your debit card, or make a bank transfer to the TransferWise bank account in your currency.
                            </div>
                        </li>
                        <li class="col30">
                            <div class="wrap-hiw">
                                <div class="media-img">
                                    <img src="/static/images/media-block3.png">
                                </div>
                                <h3>Payment received</h3>
                                TransferWise makes the conversion and pays the recipient in the required currency. That might be a person, a company, or your own overseas account.
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="saving top20">
            <div class="wrap-text">
                <div class="bot10"><input class="button glow green big submitCalculatorForm event_index_make_a_transfer" type="button" value="make a payment"></div>
                In short, you make a <span class="source-currency-symbol b">£</span>
                <span class="source-value b">1,000.00</span>
                <span class="b">deposit</span> to us, we convert it to
                <span class="target-currency-symbol b">€</span>
                <span class="target-value b">1,189.86</span>, deliver it to the recipient within
                <span class="b">1-3 working days</span> and you save
                <span class="saving-currency-symbol b">€</span>
                <span class="saving-value b">53.21</span>. That's pretty wise, don't you think?
            </div>
            <div class="make-transferwise">
                <span class="block-desktop block-tablet">
                    <p class="text">
                        If this sounds reasonable you can start,
                        or watch a
                        <a href="https://www.youtube.com/embed/ztN7gSqr2HM?rel=0&vq=hd720" class="iframe youtube">walkthrough video</a>
                        for more info.
                    </p>
                </span>
            </div>
        </div>
        <div class="clear"></div>
        </div>
        </div>

        <div class="trustpilot">
            <div class="container">
                <h4>Top #2 trusted money service in the UK according to Trustpilot reviews</h4>
                Reviews from people who ditched their bank for TransferWise
                <div class="blocks">
                    <div class="flexslider-trustpilot">
                        <ul class="slides top30 event_trustpilot_reviews">
                            <li class="col30">
                                <div class="wrap-plt">
                                    <div class="wrap-stars"><div class="progress-stars" style="width: 100%;"></div></div>
                                    <h4 class="title"><a href="http://www.trustpilot.co.uk/review/transferwise.com#7062171" target="_blank">Used twice, perfect both times!</a></h4>
                                    <span class="content">i can not rate this service highly enough! I have now used it twice for amounts of 60 and 300euros. Both times it has worked with very little effort, very fast and seemingly very secure. Emails are received every step of the way so you know exactly where you're at. on the 300euro transfer with the saving of fees and the much better ER, i think I saved roughly 30GBP. Thats got to be worth it!</span>
                                    <p class="author-trustpilot top5">Adrian, 4 days ago</p>
                                </div>
                            </li>
                            <li class="col30">
                                <div class="wrap-plt">
                                    <div class="wrap-stars"><div class="progress-stars" style="width: 100%;"></div></div>
                                    <h4 class="title"><a href="http://www.trustpilot.co.uk/review/transferwise.com#7059770" target="_blank">Essential, and perfect in what it does</a></h4>
                                    <span class="content">TransferWise allows you to, very quickly, perform cross-devise money transfers. The fees are so low that no bank can compete.</span>
                                    <p class="author-trustpilot top5">Arthur L., 4 days ago</p>
                                </div>
                            </li>
                            <li class="col30">
                                <div class="wrap-plt">
                                    <div class="wrap-stars"><div class="progress-stars" style="width: 100%;"></div></div>
                                    <h4 class="title"><a href="http://www.trustpilot.co.uk/review/transferwise.com#7059183" target="_blank">Faultless Process!</a></h4>
                                    <span class="content">This is so easy to do once you're registered and have set up your personal details. Good value too....I have been using another Currency Converter (HIFX), but Transferwise gives me a full 2 centimes more per pound. On my last transaction, that was a benefit to me of over 80 euros. Thank you!</span>
                                    <p class="author-trustpilot top5">John, 4 days ago</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clear"></div>
                </div>

                <ul class="reviews">
                    <li class="customer">Customer&nbsp;reviews&nbsp;<span class="value">2672</span></li>
                    <li class="trustscore">Trustscore&nbsp;<span class="value">9.7</span>&nbsp;<span class="max-value">of 10</span></li>
                    <li class="rating">Star rating&nbsp;<span class="value">5</span>&nbsp;<span class="max-value">of 5</span></li>
                </ul>
                <div class="wrap-reviews">
                    <div class="wrap-button">
                        <a class="button all-reviews event_see_all_reviews" target="_blank" href="http://www.trustpilot.co.uk/review/transferwise.com">See all reviews</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="home-media-fb">
            <div class="social">
                <div class="container fb">
                    <h4>Still not convinced?</h4>
                    <hr class="mtop30 mbot20">
                    <div class="center">
                        <p class="bline bottom">Your friends probably use TransferWise to transfer money already. Ask them about it and the money they’ve saved.</p>
                        <div class="social facebook">
                            <div class="fb-like-box fb_iframe_widget" data-href="http://www.facebook.com/transferwise" data-width="980" data-height="" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=120387148077990&amp;color_scheme=light&amp;header=false&amp;href=http%3A%2F%2Fwww.facebook.com%2Ftransferwise&amp;locale=en_US&amp;sdk=joey&amp;show_border=false&amp;show_faces=true&amp;stream=false&amp;width=980"><span style="vertical-align: bottom; width: 980px; height: 241px;">
                                <iframe name="f13e558cec" width="980px" height="1000px" frameborder="0" allowtransparency="true" scrolling="no" title="fb:like_box Facebook Social Plugin" src="/static/templates/like_box.html" style="border: none; visibility: visible; width: 980px; height: 241px;" class=""></iframe></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="display: none">
        <div class="cur-wish-dialog" id="cur-wish-dialog">
        <a href="https://transferwise.com/#cur-wish-dialog" id="fire-cur-wish-pop" class="hide">fire</a>
        <a href="https://transferwise.com/#" id="close-cur-wish-pop" class="close-cur-wish-pop">x</a>
        <form onsubmit="jQuery.ajax({type:&#39;POST&#39;,data:jQuery(this).serialize(), url:&#39;/captureUserData/captureCurrencyWish&#39;,success:function(data,textStatus){jQuery(&#39;#cur-wish-dialog&#39;).html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false" method="post" action="https://transferwise.com/captureUserData/captureCurrencyWish" id="curWishListForm">
            <h3>New currencies are coming soon to TransferWise.</h3>
            <div class="main-body">
                <div class="regular-p mtop10">Want to be first to know?</div>
                <div class="regular-p">Enter a few details below and you'll receive an email when there's good news.</div>
                <hr>
                <ul class="select-row">
                    <li class="source-cell">
                        <label>I want to send money from</label>
                        <div class="cur-background ">
                            <select name="currencyFrom" class="currencyFrom" id="currencyFrom" style="display: none;" aria-disabled="false">
                                <option value="" selected="selected">Select currency</option>
                                <option value="AUD">Australian dollar</option>
                                <option value="BDT">Bangladeshi Taka</option>
                                <option value="BRL">Brazil Real</option>
                                <option value="BGN">Bulgarian Lev</option>
                                <option value="CAD">Canadian dollar</option>
                                <option value="CLP">Chile Peso</option>
                                <option value="CNY">Chinese yuan</option>
                                <option value="COP">Colombia Peso</option>
                                <option value="CRC">Costa Rica Colon</option>
                                <option value="CZK">Czech Koruna</option>
                                <option value="DKK">Danish krone</option>
                                <option value="ETB">Ethiopian birr</option>
                                <option value="EUR">Euro</option>
                                <option value="GEL">Georgian lari</option>
                                <option value="GHC">Ghana Cedis</option>
                                <option value="GTQ">Guatemala Quetzal</option>
                                <option value="HKD">Hong Kong Dollar</option>
                                <option value="HUF">Hungarian Forint</option>
                                <option value="ISK">Icelandic króna</option>
                                <option value="INR">Indian Rupee</option>
                                <option value="IDR">Indonesia Rupiah</option>
                                <option value="IDR">Indonesian Rupiah</option>
                                <option value="ILS">Israeli shekel</option>
                                <option value="JPY">Japanese yen</option>
                                <option value="KES">Kenyan Shilling</option>
                                <option value="KRW">Korea (South) Won</option>
                                <option value="KGS">Kyrgyzstan Som</option>
                                <option value="LVL">Latvian Lats</option>
                                <option value="LBP">Lebanon Pound</option>
                                <option value="LRD">Liberia Dollar</option>
                                <option value="LTL">Lithuanian Litas</option>
                                <option value="MYR">Malaysian Ringgit</option>
                                <option value="MYR">Malaysian Ringgit</option>
                                <option value="MXN">Mexican Peso</option>
                                <option value="MXN">Mexico Peso</option>
                                <option value="MAD">Moroccan Dirham</option>
                                <option value="RON">New Romanian Leu</option>
                                <option value="TRY">New Turkish Lira</option>
                                <option value="NZD">New Zealand Dollar</option>
                                <option value="NGN">Nigeria Naira</option>
                                <option value="NOK">Norwegian krone</option>
                                <option value="PKR">Pakistan Rupee</option>
                                <option value="PEN">Peru Nuevo Sol</option>
                                <option value="PHP">Philippine Peso</option>
                                <option value="PLN">Polish Zloty</option>
                                <option value="GBP">Pound Sterling</option>
                                <option value="QAR">Qatar Riyal</option>
                                <option value="RUB">Russian Rouble</option>
                                <option value="SAR">Saudi Arabia Riyal</option>
                                <option value="CFA">Seneral</option>
                                <option value="SGD">Singapore Dollar</option>
                                <option value="ZAR">South African Rand</option>
                                <option value="KRW">South Korean Won</option>
                                <option value="LKR">Sri Lanka Rupee</option>
                                <option value="SDG">Sudanese Pound</option>
                                <option value="SEK">Swedish krona</option>
                                <option value="CHF">Swiss franc</option>
                                <option value="TWD">Taiwan New Dollar</option>
                                <option value="THB">Thai Baht</option>
                                <option value="UGX">Ugandan Shilling</option>
                                <option value="AED">United Arab Emirates Dirham</option>
                                <option value="USD">United States dollar</option>
                                <option value="VND">Viet Nam Dong</option>
                            </select><div><a class="ui-selectmenu ui-widget ui-state-default ui-corner-all ui-selectmenu-dropdown currencyFrom" id="currencyFrom-button" role="button" href="https://transferwise.com/#nogo" tabindex="0" aria-haspopup="true" aria-owns="currencyFrom-menu" aria-disabled="false" style="width: 260px;"><span class="ui-selectmenu-status">Select currency</span><span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span></a></div>
                        </div>
                    </li>
                    <li class="arrow-cell"></li>
                    <li class="target-cell">
                        <label>To</label>
                        <div class="cur-background ">
                            <select name="currencyTo" class="currencyTo" id="currencyTo" style="display: none;" aria-disabled="false">
                                <option value="" selected="selected">Select currency</option>
                                <option value="AUD">Australian dollar</option>
                                <option value="BDT">Bangladeshi Taka</option>
                                <option value="BRL">Brazil Real</option>
                                <option value="BGN">Bulgarian Lev</option>
                                <option value="CAD">Canadian dollar</option>
                                <option value="CLP">Chile Peso</option>
                                <option value="CNY">Chinese yuan</option>
                                <option value="COP">Colombia Peso</option>
                                <option value="CRC">Costa Rica Colon</option>
                                <option value="CZK">Czech Koruna</option>
                                <option value="DKK">Danish krone</option>
                                <option value="ETB">Ethiopian birr</option>
                                <option value="EUR">Euro</option>
                                <option value="GEL">Georgian lari</option>
                                <option value="GHC">Ghana Cedis</option>
                                <option value="GTQ">Guatemala Quetzal</option>
                                <option value="HKD">Hong Kong Dollar</option>
                                <option value="HUF">Hungarian Forint</option>
                                <option value="ISK">Icelandic króna</option>
                                <option value="INR">Indian Rupee</option>
                                <option value="IDR">Indonesia Rupiah</option>
                                <option value="IDR">Indonesian Rupiah</option>
                                <option value="ILS">Israeli shekel</option>
                                <option value="JPY">Japanese yen</option>
                                <option value="KES">Kenyan Shilling</option>
                                <option value="KRW">Korea (South) Won</option>
                                <option value="KGS">Kyrgyzstan Som</option>
                                <option value="LVL">Latvian Lats</option>
                                <option value="LBP">Lebanon Pound</option>
                                <option value="LRD">Liberia Dollar</option>
                                <option value="LTL">Lithuanian Litas</option>
                                <option value="MYR">Malaysian Ringgit</option>
                                <option value="MYR">Malaysian Ringgit</option>
                                <option value="MXN">Mexican Peso</option>
                                <option value="MXN">Mexico Peso</option>
                                <option value="MAD">Moroccan Dirham</option>
                                <option value="RON">New Romanian Leu</option>
                                <option value="TRY">New Turkish Lira</option>
                                <option value="NZD">New Zealand Dollar</option>
                                <option value="NGN">Nigeria Naira</option>
                                <option value="NOK">Norwegian krone</option>
                                <option value="PKR">Pakistan Rupee</option>
                                <option value="PEN">Peru Nuevo Sol</option>
                                <option value="PHP">Philippine Peso</option>
                                <option value="PLN">Polish Zloty</option>
                                <option value="GBP">Pound Sterling</option>
                                <option value="QAR">Qatar Riyal</option>
                                <option value="RUB">Russian Rouble</option>
                                <option value="SAR">Saudi Arabia Riyal</option>
                                <option value="CFA">Seneral</option>
                                <option value="SGD">Singapore Dollar</option>
                                <option value="ZAR">South African Rand</option>
                                <option value="KRW">South Korean Won</option>
                                <option value="LKR">Sri Lanka Rupee</option>
                                <option value="SDG">Sudanese Pound</option>
                                <option value="SEK">Swedish krona</option>
                                <option value="CHF">Swiss franc</option>
                                <option value="TWD">Taiwan New Dollar</option>
                                <option value="THB">Thai Baht</option>
                                <option value="UGX">Ugandan Shilling</option>
                                <option value="AED">United Arab Emirates Dirham</option>
                                <option value="USD">United States dollar</option>
                                <option value="VND">Viet Nam Dong</option>
                            </select><div><a class="ui-selectmenu ui-widget ui-state-default ui-corner-all ui-selectmenu-dropdown  currencyTo" id="currencyTo-button" role="button" href="https://transferwise.com/#nogo" tabindex="0" aria-haspopup="true" aria-owns="currencyTo-menu" aria-disabled="false" style="width: 260px;"><span class="ui-selectmenu-status">Select currency</span><span class="ui-selectmenu-icon ui-icon ui-icon-triangle-1-s"></span></a></div>
                        </div>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="top20 name-wrap">
                <label for="name">Name</label>
                <input type="text" name="name" class="" id="name" value="">
            </div>
            <div class="left top20">
                <label for="emailAddress">Email Address</label>
                <input type="email" name="email" value="" class="left mright20 " id="emailAddress" placeholder="name@domain.com">
                <input type="submit" name="done" class="button big green left all-done" value="All done" id="done">
            </div>
            <div class="clear"></div>
        </form>
        <script>
            $(function() {
                var CurrencyWishesHandler = function(block) {
                    this.block = block;
                    this.curFromSelect = this.block.find('select[name="currencyFrom"]');
                    this.curToSelect = this.block.find('select[name="currencyTo"]');
                    this.closePopUpLink = this.block.find('a#close-cur-wish-pop');
                    this.firePopUpLink = this.block.find('a#fire-cur-wish-pop');
                    this.submitButton = this.block.find('input.all-done');
                    this.init();
                };
                CurrencyWishesHandler.prototype = {
                    init: function() {
                        var self = this;
                        //this.curFromSelect.selectmenu();
                        //this.curToSelect.selectmenu();
                        this.closePopUpLink.click(function(){
                            $.fancybox.close();
                            LogUtils.sendGoogleAnalyticsEvent('CurrencyWishesPopUp', 'initial-screen-click-close');
                        });
                        this.firePopUpLink.fancybox({
                            'showCloseButton': false,
                            'hideOnOverlayClick' : false,
                            'overlayOpacity': 0.5,
                            padding: 0,
                            onStart: function(){
                                $("#fancybox-outer, #fancybox-bg-n, #fancybox-bg-ne, #fancybox-bg-e, #fancybox-bg-se," +
                                        "#fancybox-bg-s, #fancybox-bg-sw, #fancybox-bg-w, #fancybox-bg-nw").css("background","none");
                            }
                        });
                        this.submitButton.click(function() {
                            LogUtils.sendGoogleAnalyticsEvent('CurrencyWishesPopUp', 'initial-screen-all-done-click');
                        });
                    }
                };
                new CurrencyWishesHandler(jQuery('.cur-wish-dialog'));
            });
        </script>
        </div>
        </div>
        <br class="clear">
        <div class="footer">
            <div class="container image">
                <div class="column">
                    <div class="footer-title">About Us</div>
                    <div class="items">
                        <ul>
                            <li><a href="https://transferwise.com/about">Company &amp; Team</a></li>
                            <li><a href="https://transferwise.com/blog">News &amp; Blog</a></li>
                            <li><a href="https://transferwise.com/media-and-awards">Press</a></li>
                            <li><a href="https://transferwise.com/jobs">Careers</a></li>
                            <li><a href="https://transferwise.com/students">TransferWise for Students</a></li>
                            <li><a href="https://transferwise.com/oxfam">Donate to Oxfam</a></li>
                        </ul>
                    </div>
                </div>
                <div class="column">
                    <div class="footer-title">Help &amp; Support</div>
                    <div class="items">
                        <ul>
                            <li><a href="https://transferwise.com/support/how-transferwise-works">Getting started</a></li>
                            <li><a href="https://transferwise.com/support/how-much-does-it-cost">Pricing</a></li>
                            <li><a href="https://transferwise.com/support/what-currencies">Supported currencies</a></li>
                            <li><a href="https://transferwise.com/cheapest-money-transfer-guaranteed">Best Price Guarantee</a></li>
                            <li><a href="https://transferwise.com/support">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="column">
                    <div class="footer-title">Countries &amp; Currencies</div>
                    <div class="items">
                        <ul class="countries-list-expandable">
                            <li><a href="https://transferwise.com/transfer-money-to-the-uk">Transfer money to the UK</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-the-usa">Transfer money to the USA</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-france">Transfer money to France</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-spain">Transfer money to Spain</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-poland">Transfer money to Poland</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-europe">Transfer money to Europe</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-italy">Transfer money to Italy</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-germany">Transfer money to Germany</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-ireland">Transfer money to Ireland</a></li>
                            <li><a href="https://transferwise.com/transfer-money-to-portugal">Transfer money to Portugal</a></li>
                        </ul>
                        <a href="https://transferwise.com/#" class="see-countries expand">See all the countries<span></span></a>
                    </div>
                </div>
                <div class="column-last">
                    <div class="footer-title">Contact</div>
                    <div class="items">
                        <ul class="contact-us">
                            <li><a href="mailto:support@transferwise.com" class="icon mail">support@transferwise.com</a></li>
                            <li><span class="icon phone">call +44 208 1234 020</span></li>
                            <li><span class="icon fax">fax +44 20 3318 2623</span></li>
                            <li><a href="https://twitter.com/transferwise" target="blank" class="icon twitter">Twitter</a></li>
                            <li><a href="http://www.facebook.com/transferwise" target="blank" class="icon facebook">Facebook</a>
                            </li><li><a href="https://plus.google.com/104977458125119375235/posts" rel="publisher" target="blank" class="icon gplus">Google+</a></li>
                        </ul>
                    </div>
                </div>

                <div class="line"></div>
                <div class="left bot30">
                    <p class="copyright">
                        © TransferWise Ltd 2013
                        - <a href="https://transferwise.com/terms-of-use">Terms of use</a>
                        - <a href="https://transferwise.com/privacy-policy">Privacy policy</a>
                        - <a href="https://transferwise.com/cookie-policy">Cookie policy</a>
                    </p>
                    <p class="copyright">
                        Transferwise is a registered money service business. Her Majesty's Revenue and Customs certificate number <a href="https://transferwise.com/mlr" onclick="window.open(this.href); return false;">12591871</a>.<br>
                        We are fully authorised by the UK Financial Conduct Authority (FCA) as a payments institution with reference <a href="http://www.fsa.gov.uk/register/psdFirmRefSearch.do?firmRef=571669" onclick="window.open(this.href); return false;">571669</a>.
                    </p>
                </div>
            </div>
            <div class="clear"></div>
        </div>

        <noscript>&lt;img src="//tl.r7ls.net/unscripted/26091" width="1" height="1"&gt;</noscript>

        <script type="text/javascript">
            //	document.write(unescape("%3Cscript src='" + ((document.location.protocol=="https:")?"https://snapabug.appspot.com":"http://www.snapengage.com") + "/snapabug.js' type='text/javascript'%3E%3C/script%3E"));
            (function() {
                var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
                se.src = '//commondatastorage.googleapis.com/code.snapengage.com/js/2284c885-c54d-47fb-8b5c-3f879a9ba526.js';
                var done = false;
                se.onload = se.onreadystatechange = function() {
                    if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
                        done = true;
                        // >>>>>>> Place your SnapEngage API code here <<<<<<<

                        SnapABug.setCallback('Open', function(agent) {
                            LogUtils.sendGoogleAnalyticsEventWithOption('Chat', 'live chat button is clicked', window.location.host + window.location.pathname);
                        });
                        SnapABug.setCallback('StartChat', function(email, msg, type) {
                            LogUtils.sendGoogleAnalyticsEventWithOption('Chat', 'chat is started', window.location.host + window.location.pathname);
                        });

                        if (typeof CalculatorHelpChatOpener != 'undefined') {
                            new CalculatorHelpChatOpener({selectors: [jQuery('select[name="sourceCurrencyId"]')]});
                        }
                    }
                };
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
            })();
        </script>


        <script type="text/javascript">
            jQuery('#calculateForm select').selectmenu()
            jQuery.format.locale({
                number: {
                    format: '#,###.0#',
                    groupingSeparator: ',',
                    decimalSeparator: '.'
                }
            });

            var payments = [{"amount":"700","sourceCurrencySymbol":"£","targetCurrencySymbol":"A$","countryCode":"AU","time":"moments ago"},{"amount":"167","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"ES","time":"moments ago"},{"amount":"70k","sourceCurrencySymbol":"Ft","targetCurrencySymbol":"£","countryCode":"GB","time":"moments ago"},{"amount":"1000","sourceCurrencySymbol":"£","targetCurrencySymbol":"CHF","countryCode":"CH","time":"moments ago"},{"amount":"335","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"NL","time":"8 minutes ago"},{"amount":"2500","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"ES","time":"9 minutes ago"},{"amount":"100","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"ES","time":"10 minutes ago"},{"amount":"359","sourceCurrencySymbol":"€","targetCurrencySymbol":"£","countryCode":"GB","time":"11 minutes ago"},{"amount":"1000","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"IE","time":"13 minutes ago"},{"amount":"700","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"FR","time":"13 minutes ago"},{"amount":"255","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"ES","time":"15 minutes ago"},{"amount":"500","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"ES","time":"17 minutes ago"},{"amount":"500","sourceCurrencySymbol":"€","targetCurrencySymbol":"£","countryCode":"GB","time":"22 minutes ago"},{"amount":"600","sourceCurrencySymbol":"£","targetCurrencySymbol":"₹","countryCode":"IN","time":"26 minutes ago"},{"amount":"136","sourceCurrencySymbol":"£","targetCurrencySymbol":"CHF","countryCode":"CH","time":"27 minutes ago"},{"amount":"200","sourceCurrencySymbol":"£","targetCurrencySymbol":"€","countryCode":"GR","time":"28 minutes ago"},{"amount":"300","sourceCurrencySymbol":"€","targetCurrencySymbol":"£","countryCode":"GB","time":"29 minutes ago"},{"amount":"182","sourceCurrencySymbol":"€","targetCurrencySymbol":"£","countryCode":"GB","time":"30 minutes ago"}];
            var mapHandler = new MapHandler(jQuery('.dynamic-map-holder'), payments, {});

            var HomePageVideo = function(block) {
                this.block = block;
                this.coverWrap = this.block.find('#cover-pic-wrap');
                this.videoWrap = this.block.find('#thevideo');
                this.youTubeIframe = this.block.find('#youTubeIframe');

                this.init();
            };

            HomePageVideo.prototype = {
                init: function() {
                    var self = this;

                    this.coverWrap.click(function(e) {
                        self.playVideo(e);
                    });
                },
                playVideo: function() {
                    var self = this;
                    this.coverWrap.hide();
                    this.videoWrap.fadeIn('fast');
                    this.youTubeIframe.attr('src', self.youTubeIframe.attr('src') + '&autoplay=1');
                }
            };

            new HomePageVideo(jQuery('.player'));

            function quote(element) {
                var quoteToReplace = $('.quote');
                var authorToReplace = $('.author');
                var partners = $('.partners a');
                var linkId = element.attr('id');
                if (linkId) {
                    var quote = $('a#'+ linkId +' .quote-text').val();
                    var author = $('a#'+ linkId +' .author-text').val();
                    quoteToReplace.replaceWith('<p class="quote"><span class="symbol">&#8220;</span>'+ quote +'<span class="symbol">&#8220;</span></p>');
                    authorToReplace.replaceWith('<p class="author">'+ author +'</p>');
                }
                partners.removeClass('active');
                if (jQuery(window).width() >= 768) {
                    element.addClass('active');
                }
            }

            $('.partners a').live('hover', function() {
                quote($(this));
            });

            jQuery('.event_partners a').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('partners', 'click_partner_' + jQuery(this).find('input#author').attr('value').replace(' ', '_'));
            });

            $.ajax({
                url: 'https://ssl.trustpilot.com/tpelements/1538723/f.jsonp',
                dataType: 'jsonp',
                jsonp: true,
                jsonpCallback: 'trustpilot_jsonp_callback',
                success: function(data, status) {
                    jQuery('.customer .value').text(data.ReviewCount.Total);
                    jQuery('.trustscore .value').text(data.TrustScore.Score / 10);
                    jQuery('.rating .value').text(data.TrustScore.Stars);

                    jQuery('.wrap-plt').each( function(index) {
                        var review = data.Reviews[index];
                        jQuery('.progress-stars').eq(index).css('width', review.TrustScore.Score + '%');
                        jQuery('.wrap-plt').find('.title a').eq(index).text(review.Title).attr('href', review.Url);
                        jQuery('.wrap-plt').find('.content').eq(index).text(review.Content);
                        jQuery('.wrap-plt').find('.author-trustpilot').eq(index).text(review.User.Name + ', ' + moment(review.Created.Human).fromNow()) ;
                    });
                },
                error: function(XHR, txtStatus, errThrown) {
                    console.log('Error: ' + txtStatus);
                    console.log('Error: ' + errThrown);
                }
            });

            jQuery('.event_trustpilot_reviews h4.title a').click(function() {
                var url = jQuery(this).attr('href');
                LogUtils.sendGoogleAnalyticsEvent('trustpilot_reviews', 'click_review_id_' + url.substr(url.indexOf('#') + 1));
            });

            jQuery('.event_see_all_reviews').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('trustpilot_reviews', 'click_all_reviews_button');
            });

            jQuery('.event_index_watch_video').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('watch_the_video', 'click_watch_video_button');
            });

            $('.flexslider-stories').flexslider({
                animation: "slide",
                controlNav: true,
                directionNav: true,
                touch: true,
                slideshowSpeed: 12000,
                slideshow: true,
                useCSS: false
            });

            var InitiateResponsiveHome = new InitiateResponsiveHome();

            window.fbAsyncInit = function() {
                FB.init({appId: '120387148077990', status: true, cookie: true, xfbml: true});
                _ga.trackFacebook();
            };

            var youTubeVideoLink = jQuery("a.iframe.youtube");
            var properWidth = document.body.offsetWidth - 60;

            if(document.body.offsetWidth < 960 && document.body.offsetWidth > 480){
                youTubeVideoLink.fancybox({
                    'scrolling': 'no',
                    'padding' : 0,
                    'autoScale': false
                });
                console.log('das')
            } else if (document.body.offsetWidth < 480) {
                youTubeVideoLink.fancybox({
                    'width': properWidth,
                    'scrolling': 'no',
                    'padding' : 0,
                    'autoScale': false
                });
            } else {
                youTubeVideoLink.fancybox({
                    'width': '83',
                    'height': '47',
                    'scrolling': 'no',
                    'padding' : 0,
                    'autoScale': false
                });
            }

            jQuery('.event_index_make_a_transfer').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('make_transfer', 'click_bottom_button');
            });
            LogUtils.trackPageView("front page", {landing: 'main'});
            jQuery.scrollDepth({
                percentage: false,
                elements: {'Footer': '.footer'},
                eventName: 'Index Page Scroll'
            })

            var LanguageSelector = function() {
                this.listOfLanguages = $('.header ul.language-list-ul');
                this.chooseLanguageLink = $('.header .choose-link');
                this.init();
            }

            LanguageSelector.prototype = {
                init: function() {
                    var self = this;
                    this.chooseLanguageLink.click(function(e) {
                        self.openLanguageList(e);
                    });
                },

                openLanguageList: function(e) {
                    e.preventDefault();
                    this.listOfLanguages.toggle('fast');
                }
            }

            $(document).ready(function(){
                var languageSelect = new LanguageSelector();
            });

            $('.header .nav-menu-button').bind('click', function() {
                $('.header .login').slideToggle('slow');
            });

            LogUtils.signUpClick('.event-signup-click');

            $(function() {
                new GreetingMessageManager(jQuery('.login'), 'en');
            });

            $('.header .nav-menu-button').bind('click', function() {
                $('.header .nav').slideToggle('slow');
            });

            LogUtils.signUpClick('.event-signup-click');

            jQuery('.event_account_menu').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('account_link', 'click_menu');
            });

            jQuery('.event_account_link').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('account_link', 'click_name_link');
            });

            jQuery('.event_logo').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('logo', 'click_logo');
            });

            jQuery('.event_how_it_works').click(function() {
                LogUtils.sendGoogleAnalyticsEvent('how_it_works', 'click_menu');
            });

            jQuery(function() {
                var s = document.createElement('script'),
                        e = document.getElementsByTagName('script')[0];
                s.src = '//d3cxv97fi8q177.cloudfront.net/foundation-A26091-0f7a-4f32-8a77-ffc0d8bc79312.js';s.type = 'text/javascript';s.async = true;
                e.parentNode.insertBefore(s, e);
            });

            var ExpandableCountryList = function(block) {
                this.block = block;
                this.expandableList = this.block.find('.countries-list-expandable');
                this.expandTrigger = this.block.find('.see-countries');
                this.init();
            };

            ExpandableCountryList.prototype = {
                init: function() {
                    var self = this;

                    this.expandTrigger.toggle(function(e){
                        e.preventDefault();
                        self.expandableList.animate({height:200},200);
                        $(this).toggleClass('expand').toggleClass('suspend');
                    },function(e){
                        e.preventDefault();
                        self.expandableList.animate({height:100},200);
                        $(this).toggleClass('expand').toggleClass('suspend');
                    });
                }
            };

            var expandableCountryList = new ExpandableCountryList(jQuery('.footer'));

            adroll_adv_id = "YK4OBXO2MZBELGDAGD343A";
            adroll_pix_id = "26356KPY2VD5TBHX4OFVL7";
            (function () {
                var oldonload = window.onload;
                window.onload = function(){
                    __adroll_loaded=true;
                    var scr = document.createElement("script");
                    var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                    scr.setAttribute('async', 'true');
                    scr.type = "text/javascript";
                    //scr.src = host + "/j/roundtrip.js";
                    ((document.getElementsByTagName('head') || [null])[0] ||
                            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                    if(oldonload){oldonload()}};
            }());

            if (jQuery.validator) {
                jQuery.extend(jQuery.validator.messages, {
                    required: 'We need this information.',
                    remote: 'Please fix this field.',
                    email: 'Please enter a valid email address.',
                    url: 'Please enter a valid URL.',
                    date: 'Please enter a valid date.',
                    dateISO: 'Please enter a valid date (ISO).',
                    number: 'Please enter a valid number.',
                    digits: 'Please enter only digits.',
                    creditcard: 'Please enter a valid credit card number.',
                    equalTo: 'Please enter the same value again.',
                    accept: 'Please enter a value with a valid extension.',
                    maxlength: jQuery.validator.format('Please enter no more than {0} characters.'),
                    minlength: jQuery.validator.format('Please enter at least {0} characters.'),
                    rangelength: jQuery.validator.format('Please enter a value between {0} and {1} characters long.'),
                    range: jQuery.validator.format('Please enter a value between {0} and {1}.'),
                    max: jQuery.validator.format('Please enter a value less than or equal to {0}.'),
                    min: jQuery.validator.format('Please enter a value greater than or equal to {0}.')
                });
            }
        </script>
        <div class="ui-selectmenu-menu" style="z-index: 501; top: 249px; left: 961px;">
            <ul class="ui-widget ui-widget-content ui-selectmenu-menu-dropdown ui-corner-bottom currencySelect sourceCurrency showScrollOSX" aria-hidden="true" role="listbox" aria-labelledby="ui-selectmenu-728dcf72-button" id="ui-selectmenu-728dcf72-menu" style="width: 115px; height: 188.66666666666666px;" aria-disabled="false" aria-activedescendant="ui-selectmenu-item-188">
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">EUR</a></li>
                <li role="presentation" class="ui-selectmenu-item-selected"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="true" id="ui-selectmenu-item-188">GBP</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">PLN</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">CHF</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">NOK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">SEK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">DKK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">HUF</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">GEL</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">RON</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">TRY</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">CZK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">BGN</a></li>
                <li role="presentation" class="other-cur-option ui-corner-bottom"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Other</a></li>
            </ul>
        </div>
        <div class="ui-selectmenu-menu" style="z-index: 501; top: 339px; left: 961px;">
            <ul class="ui-widget ui-widget-content ui-selectmenu-menu-dropdown ui-corner-bottom currencySelect targetCurrency showScrollOSX" aria-hidden="true" role="listbox" aria-labelledby="ui-selectmenu-e9b1e618-button" id="ui-selectmenu-e9b1e618-menu" style="width: 115px; height: 188.66666666666666px;" aria-disabled="false" aria-activedescendant="ui-selectmenu-item-870">
                <li role="presentation" class="ui-selectmenu-item-selected"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="true" id="ui-selectmenu-item-870">EUR</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">GBP</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">USD</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">PLN</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">CHF</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">NOK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">SEK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">DKK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">AUD</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">HUF</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">GEL</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">HKD</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">INR</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">RON</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">TRY</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">NZD</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">SGD</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">ZAR</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">CZK</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">BGN</a></li>
                <li role="presentation" class="other-cur-option ui-corner-bottom"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Other</a></li>
            </ul>
        </div>
        <script type="text/javascript">
            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
            $(function() {
                setMixPanelCookies();
            });
            function setMixPanelCookies() {
                if(!mixpanel.get_distinct_id) {
                    setTimeout(setMixPanelCookies, 50);
                    return;
                }
                // TODO think about to remove this cookie functionality and use cookie provided by mixpanel
                $.cookie('analyticsUserId', mixpanel.get_distinct_id(), { path: '/'});
            }
        </script>

        <noscript>&lt;img src="//tl.r7ls.net/unscripted/26091" width="1" height="1"&gt;</noscript>

        <iframe src="/static/templates/saved_resource.html" width="1" height="1" border="0" scrolling="no" marginheight="0" marginwidth="0" frameborder="0">
        </iframe>

        <div class="ui-selectmenu-menu" style="z-index: 1; top: 38px; left: 0px;">
            <ul class="ui-widget ui-widget-content ui-selectmenu-menu-dropdown ui-corner-bottom currencyFrom" aria-hidden="true" role="listbox" aria-labelledby="currencyFrom-button" id="currencyFrom-menu" style="width: 260px; height: 188.66666666666666px;" aria-disabled="false" aria-activedescendant="ui-selectmenu-item-698">
                <li role="presentation" class="ui-selectmenu-item-selected"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="true" id="ui-selectmenu-item-698">Select currency</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Australian dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Bangladeshi Taka</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Brazil Real</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Bulgarian Lev</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Canadian dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Chile Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Chinese yuan</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Colombia Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Costa Rica Colon</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Czech Koruna</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Danish krone</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Ethiopian birr</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Euro</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Georgian lari</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Ghana Cedis</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Guatemala Quetzal</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Hong Kong Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Hungarian Forint</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Icelandic króna</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Indian Rupee</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Indonesia Rupiah</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Indonesian Rupiah</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Israeli shekel</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Japanese yen</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Kenyan Shilling</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Korea (South) Won</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Kyrgyzstan Som</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Latvian Lats</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Lebanon Pound</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Liberia Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Lithuanian Litas</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Malaysian Ringgit</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Malaysian Ringgit</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Mexican Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Mexico Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Moroccan Dirham</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">New Romanian Leu</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">New Turkish Lira</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">New Zealand Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Nigeria Naira</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Norwegian krone</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Pakistan Rupee</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Peru Nuevo Sol</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Philippine Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Polish Zloty</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Pound Sterling</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Qatar Riyal</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Russian Rouble</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Saudi Arabia Riyal</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Seneral</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Singapore Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">South African Rand</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">South Korean Won</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Sri Lanka Rupee</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Sudanese Pound</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Swedish krona</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Swiss franc</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Taiwan New Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Thai Baht</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Ugandan Shilling</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">United Arab Emirates Dirham</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">United States dollar</a></li>
                <li role="presentation" class="ui-corner-bottom"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Viet Nam Dong</a></li>
            </ul>
        </div>
        <div class="ui-selectmenu-menu" style="z-index: 1; top: 38px; left: 0px;">
            <ul class="ui-widget ui-widget-content ui-selectmenu-menu-dropdown ui-corner-bottom  currencyTo" aria-hidden="true" role="listbox" aria-labelledby="currencyTo-button" id="currencyTo-menu" style="width: 260px; height: 188.66666666666666px;" aria-disabled="false" aria-activedescendant="ui-selectmenu-item-172">
                <li role="presentation" class="ui-selectmenu-item-selected"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="true" id="ui-selectmenu-item-172">Select currency</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Australian dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Bangladeshi Taka</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Brazil Real</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Bulgarian Lev</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Canadian dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Chile Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Chinese yuan</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Colombia Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Costa Rica Colon</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Czech Koruna</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Danish krone</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Ethiopian birr</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Euro</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Georgian lari</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Ghana Cedis</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Guatemala Quetzal</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Hong Kong Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Hungarian Forint</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Icelandic króna</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Indian Rupee</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Indonesia Rupiah</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Indonesian Rupiah</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Israeli shekel</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Japanese yen</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Kenyan Shilling</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Korea (South) Won</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Kyrgyzstan Som</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Latvian Lats</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Lebanon Pound</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Liberia Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Lithuanian Litas</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Malaysian Ringgit</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Malaysian Ringgit</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Mexican Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Mexico Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Moroccan Dirham</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">New Romanian Leu</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">New Turkish Lira</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">New Zealand Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Nigeria Naira</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Norwegian krone</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Pakistan Rupee</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Peru Nuevo Sol</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Philippine Peso</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Polish Zloty</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Pound Sterling</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Qatar Riyal</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Russian Rouble</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Saudi Arabia Riyal</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Seneral</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Singapore Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">South African Rand</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">South Korean Won</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Sri Lanka Rupee</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Sudanese Pound</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Swedish krona</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Swiss franc</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Taiwan New Dollar</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Thai Baht</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Ugandan Shilling</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">United Arab Emirates Dirham</a></li>
                <li role="presentation"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">United States dollar</a></li>
                <li role="presentation" class="ui-corner-bottom"><a href="https://transferwise.com/#nogo" tabindex="-1" role="option" aria-selected="false">Viet Nam Dong</a></li>
            </ul>
        </div>
        <div id="fancybox-tmp"></div>
        <div id="fancybox-loading">
            <div></div>
        </div>
        <div id="fancybox-overlay"></div>
        <div id="fancybox-wrap">
            <div id="fancybox-outer">
                <div class="fancybox-bg" id="fancybox-bg-n"></div>
                <div class="fancybox-bg" id="fancybox-bg-ne"></div>
                <div class="fancybox-bg" id="fancybox-bg-e"></div>
                <div class="fancybox-bg" id="fancybox-bg-se"></div>
                <div class="fancybox-bg" id="fancybox-bg-s"></div>
                <div class="fancybox-bg" id="fancybox-bg-sw"></div>
                <div class="fancybox-bg" id="fancybox-bg-w"></div>
                <div class="fancybox-bg" id="fancybox-bg-nw"></div>
                <div id="fancybox-content"></div>
                <a id="fancybox-close"></a>
                <div id="fancybox-title"></div>
                <a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>
                <a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>
            </div>
        </div>
        <div id="SnapABug_W" style="position: fixed; display: none; top: 0%; left: 0px; width: 100%; height: 100%; text-align: left; z-index: 2147483644; margin: 0px; padding: 0px; border-collapse: collapse; border-spacing: 0px; border: none; list-style: none; font-size: 100%; font-weight: normal; outline: none; background-image: url(https://commondatastorage.googleapis.com/code.snapengage.com/wbg/blank.gif); background-repeat: repeat repeat;">
            <div id="SnapABug_O" style="position:absolute;width:100%;height:100%;top:0px;left:0px;background-color:white;text-align:left;z-index:auto;-moz-opacity:0.5;opacity:.50;filter:alpha(opacity=50);"></div>
        </div>
        <div id="SnapABug_WP" style="position: fixed; display: none; z-index: 2147483645; overflow: hidden; margin: 0px; padding: 0px; border-collapse: collapse; border-spacing: 0px; border: none; list-style: none; outline: none;">
            <div id="SnapABug_P" style="position:absolute;left:0px;top:0px;margin:0;padding:0;border:none;text-align:left;z-index:1;overflow:hidden;background-color:transparent;"></div>
            <div id="SnapABug_IP" style="position:absolute;left:0px;top:0px;z-index:0;"></div>
            <div id="SnapABug_DB" style="position:absolute;left:0px;top:0px;margin:0;border:0;padding:0;z-index:1;cursor:move;background-image:url(&#39;https://commondatastorage.googleapis.com/code.snapengage.com/wbg/blank.gif&#39;);background-repeat:repeat;"></div>
            <div id="SnapABug_Snd" style="width:0px;height:0px;font-size:0px;margin:0;padding:0;background-color:transparent;"></div>
        </div>
        <div id="SnapABug_Applet" style="position: fixed; display: none; top: 0%; left: 0px; width: 100%; height: 2px; text-align: left; z-index: 2147483646;"></div>
        <div id="luckyext__fl_watcher_div" style="position: absolute; z-index: 0; left: 1px; top: 1px;">
            <object style="width:1px; height:1px" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="https://transferwise.com/://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" width="1" height="1" id="luckyext__fl_saver">
                <param name="movie" value="https://ssl.luckyorange.com/save_flash.swf?nc=1">
                <param name="allowScriptAccess" value="always">
                <param name="wmode" value="transparent">
                <embed style="width:1px; height:1px" swliveconnect="true" wmode="transparent" src="https://ssl.luckyorange.com/save_flash.swf?nc=1" width="1" height="1" name="luckyext__fl_saver" allowscriptaccess="always" type="application/x-shockwave-flash" pluginspage="https:://www.macromedia.com/go/getflashplayer"></object>
        </div>
    </body>
</html>
